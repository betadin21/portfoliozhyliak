import React from "react";
import "./style.css"

export const Footer = () => {
    const data = new Date().getFullYear();
    return(
        <div className="footer__container">Created By <span className="logo">ZHYLIAKO</span>|      
        <span className="data">{data}</span>All Right Reserved.</div>
    )
}