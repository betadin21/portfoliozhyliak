import React, { useState } from "react";

import "./style.css";
import my__icon from "../../img/my__icon.svg";
import my__location from "../../img/location__icon.svg";
import my__mail from "../../img/mail__icon.svg";
const FORM_ENDPOINT = ""; // TODO - fill on the later step
export const Contact = () => {
  const [submitted, setSubmitted] = useState(false);
  const handleSubmit = () => {
    setTimeout(() => {
      setSubmitted(true);
    }, 100);
  };

  if (submitted) {
    return (
      <>
        <div className="text-2xl">Thank you!</div>
        <div className="text-md">We'll be in touch soon.</div>
      </>
    );
  }

  return (
    <div className="contact__container" id="contact">
      <div className="contact__title">Contact Me</div>
      <form action={FORM_ENDPOINT} onSubmit={handleSubmit} method="POST">
        <div className="form__title">Message Me</div>
        <div className="mb-3 pt-0">
          <input
            type="text"
            placeholder="Your name"
            name="name"
            className="px-3 py-3 placeholder-gray-400 text-gray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full"
            required
          />
        </div>
        <div className="mb-3 pt-0">
          <input
            type="email"
            placeholder="Email"
            name="email"
            className="px-3 py-3 placeholder-gray-400 text-gray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full"
            required
          />
        </div>
        <div className="mb-3 pt-0">
          <textarea
            placeholder="Your message"
            name="message"
            className="px-3 py-3 placeholder-gray-400 text-gray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full"
            required
          />
        </div>
        <div className="mb-3 pt-0">
          <button className="button" type="submit">
            Send a message ➔
          </button>
        </div>
      </form>
      <div className="get__intouch">
        <div className="touch__title">Get in Touch</div>
        <div className="touch__subtitle">
          Feel free to get in touch with me. I am always open to discussing new
          projects, creative ideas or opportunities to be part of your visions.
        </div>
        <div className="contact__data my__contactname">
          <div>
            <img src={my__icon} alt="my_icon" />
          </div>
          <span>
            <a href="#home">Oleh Zhyliak</a>
          </span>
        </div>
        <div className="contact__data my__location">
          <div>
            <img src={my__location} alt="loc_icon" />
          </div>
          <span>
            <a
              target="_blank"
              rel="noreferrer"
              href="https://www.google.com/maps/place/%D0%9A%D0%B8%D1%97%D0%B2,+02000/@50.4015698,30.2030533,10z/data=!3m1!4b1!4m6!3m5!1s0x40d4cf4ee15a4505:0x764931d2170146fe!8m2!3d50.4501!4d30.5234!16zL20vMDJzbjM0?entry=ttu"
            >
              Kyiv, Ukraine
            </a>
          </span>
        </div>
        <div className="contact__data my__mail">
          <div>
            <img src={my__mail} alt="mail_icon" />
          </div>
          <span>
            <a href="mailto:olehzh9521@gmail.com">olehzh9521@gmail.com</a>
          </span>
        </div>
      </div>
    </div>
  );
};
