import React from "react";
import myPhoto from "../../img/me.png";

import "./style.css";

export const Home = () => {
  return (
    <div className="home__container" id="home">
      <div className="home__name">
        <span className="first__string">Hi 👋, I'm</span>
        <div className="my__name">Oleh Zhyliak</div>
        <span>Frontend Developer</span>
        
        <button><a href="#contact">Contact Me ➔</a></button>
      </div>
      <div className="home__img">
        <img src={myPhoto} alt="myPhoto" />
      </div>
    </div>
  );
};
