import React,{useEffect} from "react";
import { Navigation } from "../Navigation/Navigation";
import { Home } from "../Home/Home";
import { About } from "../About/About";
import { Skills } from "../Skills/skills";
import { Projects } from "../Projects/projects";
import { Contact } from "../Contact/contact";
import { Footer } from "../Footer/footer";
import 'animate.css';
import WOW from 'wowjs';
import "./style.css"


export const App = () =>{
    useEffect(() => {
        new WOW.WOW({
          live: false
        }).init();
      }, [])
    return  (
        <>
            <Navigation/>
            <Home/>
            <About/>
            <Skills/>
            <Projects/>
            <Contact/>
            <Footer/>
        </>
    )
}