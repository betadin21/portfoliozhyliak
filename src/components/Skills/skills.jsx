import React from "react";
import { Stack } from "./StackIcon/stack";
import "./style.css";
import js from "../../img/js.png";
import html from "../../img/html.png";
import css from "../../img/css.png";
import react_icon from "../../img/react.png";
import sass from "../../img/sass.png";
import boot from "../../img/icons8-bootstrap-240.png";
import vs_code from "../../img/vs__code.png";
import jquery from "../../img/jquery.png";
import json_icon from "../../img/JSON.png";
import gitlab from "../../img/gitlab.png";
import github from "../../img/github.png";
import vercel from "../../img/vercel.png";
import tailwind from "../../img/tailwindcss.png"
export const Skills = () => {
  return (
    <div className="skills__container" id="skills">
      <div className="skills__title">My Tech Stack</div>
      <div className="stack__container">
        <Stack
          icon={js}
          text="Java Script"
          clas="stack__icon wow animate__animated animate__bounceInLeft"
        />
        <Stack
          icon={html}
          text="HTML"
          clas="stack__icon wow animate__animated animate__bounceInLeft"
        />
        <Stack
          icon={css}
          text="CSS"
          clas="stack__icon wow animate__animated animate__bounceInUp"
        />
        <Stack
          icon={react_icon}
          text="React"
          clas="stack__icon wow animate__animated animate__bounceInRight"
        />
        <Stack
          icon={sass}
          text="Sass"
          clas="stack__icon wow animate__animated animate__bounceInRight"
        />
        <Stack
          icon={boot}
          text="Bootstrap"
          clas="stack__icon wow animate__animated animate__bounceInLeft"
        />
        <Stack
          icon={vs_code}
          text="VS Code"
          clas="stack__icon wow animate__animated animate__bounceInLeft"
        />
        <Stack
          icon={jquery}
          text="jQuery"
          clas="stack__icon wow animate__animated animate__bounceInUp"
        />
        <Stack
          icon={json_icon}
          text="JSON"
          clas="stack__icon wow animate__animated animate__bounceInRight"
        />
        <Stack
          icon={gitlab}
          text="GitLab"
          clas="stack__icon wow animate__animated animate__bounceInRight"
        />
        <Stack
          icon={github}
          text="GitHub"
          clas="stack__icon wow animate__animated animate__bounceInLeft"
        />
        <Stack
          icon={vercel}
          text="Vercel"
          clas="stack__icon wow animate__animated animate__bounceInRight"
        />
        <Stack
          icon={tailwind}
          text="Tailwindcss"
          clas="stack__icon wow animate__animated animate__bounceInRight"
        />
      </div>
    </div>
  );
};
