import React from "react";

import "./style.css";



export const Stack = ({icon, text,clas}) => {

    return (
       
        <>
            <div  className={clas} >
                <img src={icon} alt="stack-icon"/>
                <div className="name__stack">{text}</div>
            </div>
        </>
    )
}