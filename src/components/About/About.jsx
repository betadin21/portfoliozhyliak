import React from "react";
import meTwo from "../../img/me2.jpg";
import downloadIcon from "../../img/download_icon.png";
import resume from "../../fileToDownload/resume.pdf"
import "./style.css";

export const About = () => {
  return (
    <div className="about__container" id="about">
      <div className="about__title">About Me</div>
      <div className="about__content">
        <div className="about__img">
          <img src={meTwo} alt="myPhoto" />
        </div>
        <div className="about__text__content">
          <div className="text__content__title">Who Am I?</div>
          <div className="text__content__subtitle">
            My passion for web development began in 2021 and from then on, I did
            everything I could to break into this path, while having fun on the
            various projects developed. Through my coursework and personal
            projects, I've gained a good grasp on coding fundamentals and have
            experience in HTML, CSS, JavaScript and React. I am especially
            interested to learn more and develop my skills in web development.
            My experience acquired over the course of the projects allows me to
            better understand the expectations of a client and to respond
            precisely to the need requested according to the field of activity.
          </div>
          <a href={resume} download className="about__button">
            <div className="load__text">Download Resume</div>
            <div className="load__icon">
              <img src={downloadIcon} alt="download" />
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};
