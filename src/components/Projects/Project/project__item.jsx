import React from "react";
import "./style.css";

export const ProjectItem = ({ title, prev_link, icon, git, tech_stack }) => {
  return (
    <div className="project__container">
      <div className="project__icon">
        <img src={icon} alt="" />
      </div>
      <div className="project__title">{title}</div>
      <div className="project__subtitle">
        This is sample project description random things are here in description
        This is sample project lorem ipsum generator for dummy content
      </div>
      <div className="tech__stack">Tech stack : {tech_stack}</div>
      <div className="project__links">
        <a href={prev_link} target="_blank" rel="noreferrer" className="preview__link">
          Live Preview
        </a>
        <a href={git} target="_blank" rel="noreferrer" className="git__link">
          View Code
        </a>
      </div>
    </div>
  );
};
