import React from "react";
import { ProjectItem } from "./Project/project__item";
import earth from "../../img/earth.png"
import cupcake from "../../img/cupcake.png"
import grocery from "../../img/grocery_list.png"

import "./style.css";

export const Projects = () =>{
    return(
        <div className="projects__container" id="projects">
            <div className="projects__title">My Projects</div>
            <div className="project__item__container">
                <ProjectItem icon={earth} tech_stack="React, JavaScript, CSS" git ="https://gitlab.com/betadin21/earth.git" prev_link="https://earth-green-rho.vercel.app/" title ="Earth Animation"/>
                <ProjectItem icon={cupcake} tech_stack="HTML, JavaScript, CSS" git ="https://gitlab.com/betadin21/project-zhyliak-1.git" prev_link="https://cupcake-shop.vercel.app/" title ="Cupcake Shop"/>
                <ProjectItem icon={grocery} tech_stack="React, JavaScript, CSS" git ="https://gitlab.com/betadin21/grocery-list.git" prev_link="https://grocery-list-livid.vercel.app/" title ="Grocery List"/>
            </div>
        </div>
    )
}