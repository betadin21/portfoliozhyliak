import React from "react";
import logo from "../../img/logo.jpg";
import ln from "../../img/ln.svg"
import styles from "./navigation.module.css";

export const Navigation = ({href}) => {
  return (
    <nav>
      <a href="#home" className={styles.logo}>
        <img src={logo} alt="logo" />
      </a>
      <ul className={styles.nav__list}>
        <li className={styles.nav__item}><a href="#home">Home</a></li>
        <li className={styles.nav__item}><a href="#about">About</a></li>
        <li className={styles.nav__item}><a href="#skills">Tech Stack</a></li>
        <li className={styles.nav__item}><a href="#projects">Projects</a></li>
        <li className={styles.nav__item}><a href="#contact">Contact</a></li>
        <li className={styles.nav__item}><a className={styles.linkedin} target="_blank" href="https://www.linkedin.com/in/oleh-zhyliak-25811027a/"><img src={ln} alt="linkedin"/><span>Linkedin</span></a></li>
        <img src="" alt="" />
      </ul>
    </nav>
  );
};
